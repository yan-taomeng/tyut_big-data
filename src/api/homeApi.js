import axios from "./http";

function getImg() {
  return axios.get("/hello");
}

function getmsg(params) {
  console.log(params);
  return axios.post("/hello", { params });
}

export default {
  //在这里导出所有函数
  getImg,
  getmsg,
};
