const path = require("path");
module.exports = {
  assetsDir: "static",
  parallel: false,
  publicPath: "./",

  pluginOptions: {
    "style-resources-loader": {
      preProcessor: "less",
      patterns: [path.resolve(__dirname, "src/themes/themes.less")],
    },
  },
};
